'use strict';

var express = require('express');
var Hapi= require('hapi');
var app = express();
var gulp = require('gulp'); // Load gulp
require('./gulpfile'); // Loads our config task
// Kick of gulp 'config' task, which generates angular const configuration
gulp.start('config'); 

// serve client side files
/*app.listen(4000);*/
var server = new Hapi.Server();
server.connection({ port: 5000});
server.start(function(){
    server.log('server running at:'+server.info.uri);
}); // uncomment this to run the server directly

console.log('Listening on port 5000');