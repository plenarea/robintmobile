'use strict';

var gulp = require('gulp');
var gulpNgConfig = require('gulp-ng-config');
var myConfig = require('./config.json');
gulp.task('config', function() {
  gulp.src('config.json')
      .pipe(gulpNgConfig('myApp.config', {
          environment: 'local'
      }))
      .pipe(gulp.dest('./assets/js/angular_files/'));

});