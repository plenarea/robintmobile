/**
 * Created by plenarea on 21/2/17.
 */
angular.module('myApp.related',[]).controller('RelatedAccessoryController',['$scope','Pagination','$accessoryService','$location','$rootScope','toaster',function ($scope,Pagination,$accessoryService,$location,$rootScope,toaster) {
    $scope.navigate=localStorage.getItem("navigate");


    /*$accessoryService.getAll().then(function (response) {

     pageData(response);

     },function(err){
     console.log("Not found Data");
     });*/

    var result1=JSON.parse(localStorage.getItem('relatedAccessory'));
    var result= JSON.parse(localStorage.getItem("viewAccessory"));
    for(var i=0;i<result1.length;i++)
    {
       for(var j=0;j<result.length;j++)
       {
           if(result1[i].id==result[j].id)
           {
               result1[i].cartqt=result[j].cartqt;
               if(result[j].visited)
               {
                   result1[i].visited=result[j].visited;
               }
           }

        }
    }
    pageData(result1);
    $scope.getDetail=function(id)
    { if(localStorage.getItem("token")==null) {
        $accessoryService.getDetailById(id).then(function (response) {
            var detailsResult=[]=response;
            var cartqt;
            var result= JSON.parse(localStorage.getItem("viewAccessory"));
            for(var i=0;i<result.length;i++)
            {
                if(result[i].id==id)
                {
                    if(result[i].cartqt==0)
                    {
                        cartqt=0;
                        break;
                    }
                    else
                    {
                        cartqt=result[i].cartqt;
                        break;
                    }

                }
            }
            for (var i=0;i<detailsResult.length;i++)
            {
                detailsResult[i].cartqt=cartqt;
            }
            localStorage.setItem('viewdetils', JSON.stringify(detailsResult));
            localStorage.setItem('product','accessory');
            $location.path('/configuration');


            for(var i=0;i<result.length;i++)
                if(result[i].id===id)
                {
                    result[i].visited=true;
                    localStorage.setItem('viewAccessory', JSON.stringify(result));
                    break;
                }

        }, function (err) {
            alert(err);
        });
    }
    else{
        return false;
    }
    };


    function pageData(response)
    {

        $scope.list=response;

        $scope.pagination = Pagination.getNew(4);
        var x=$scope.pagination.numPages=Math.ceil($scope.list.length/$scope.pagination.perPage);
        var st=[];
        for(i=0;i<x;i++)
        {
            st.push(i);
        }
        $scope.arry=st;
    };

    var cart=[];
    $scope.addCart=function (data,id) {

        if(!localStorage.getItem('cart')) {
            data.cartqt=1;
            cart.push(data);
            localStorage.setItem('cart', JSON.stringify(cart));
            var addNum;
            if($rootScope.cartnum===undefined)
            {
                addNum=0;
            }
            else {
                addNum=$rootScope.cartnum;
            }
            addNum=addNum+1;
            $rootScope.$broadcast('incCart',{"cartnum":addNum});
            localStorage.setItem('cartNum',addNum);
            var result= JSON.parse(localStorage.getItem("viewAccessory"));
            for(var i=0;i<result.length;i++)
                if(result[i].id===id)
                {
                    result[i].cartqt=1;
                    localStorage.setItem('viewAccessory', JSON.stringify(result));
                    break;
                }

            toaster.success({body:"Product Added"});

        }
        else
        {
            var test=JSON.parse(localStorage.getItem('cart'));
            data.cartqt=1;
            test.push(data);
            localStorage.setItem('cart', JSON.stringify(test));
            var addNum2;
            if($rootScope.cartnum===undefined)
            {
                addNum2=0;
            }
            else {
                addNum2=$rootScope.cartnum;
            }
            addNum2=addNum2+1;
            $rootScope.$broadcast('incCart',{"cartnum":addNum2});
            localStorage.setItem('cartNum',addNum2);
            var result1= JSON.parse(localStorage.getItem("viewAccessory"));
            for(var i=0;i<result1.length;i++)
                if(result1[i].id===id)
                {
                    result1[i].cartqt=1;
                    localStorage.setItem('viewAccessory', JSON.stringify(result1));
                    break;
                }

            toaster.success({body:"Product Added"});
        }


    };
    $scope.setSearch=function (search) {
        if($scope.search.length==0){
            $scope.accessories=null;

        }
        else {
            var keyword=$scope.search;
            $accessoryService.searchByKeyword(keyword).then(function (data) {
                $scope.accessories=data;

            })
        }
    };

    $scope.getSearch=function (search) {

        if($scope.search.length==0)
        {
            var result= JSON.parse(localStorage.getItem('searchResult'));
            pageData(result);
        }
        else
        {
            var keyword=$scope.search;
            $accessoryService.searchByKeyword(keyword).then(function (data) {

                var result= JSON.parse(localStorage.getItem('searchResult'));


                for(var i=0;i<result.length;i++) {

                    for (var j = 0; j < data.length; j++) {
                        if(result[i].id==data[j].id) {
                            data[j].cartqt = result[i].cartqt;
                            data[j].product = result[i].product;
                            if(result[i].visited)
                            {
                                data[j].visited=result[i].visited;
                            }
                        }

                    }
                }
                localStorage.removeItem('searchP');
                localStorage.setItem('searchP',JSON.stringify(data));
                localStorage.setItem('searchQuery',$scope.search);
                localStorage.setItem('localdata','accessory');
                $location.path('/search');

            })

        }
    };


    $scope.openCart=function () {
        $location.path('/cart');
    }

}]);