/**
 * Created by plenarea-2 on 27/12/16.
 */
var app = angular.module('myApp.navigate', ['ui.bootstrap']);
app.controller("NavigateController",['$scope','$routeParams','$rootScope','$AuthService','$route','$userService','Pagination','$modal','$productservice','$accessoryService','$location',function($scope,$routeParams,$rootScope,$AuthService, $route,$userService,Pagination,$modal,$productservice,$accessoryService,$location) {
    $scope.$route = $route;
    console.log($route);
    $scope.placeholderName='';
    localStorage.clear();
    $scope.$on('placeholder',function (event,data) {
        $scope.placeholderName=data;
    });

    $scope.searchText = null;
    $scope.change = function() {
        if($scope.searchText.length==0){
            $scope.entries=null;
            $scope.city=null;
        }
        else{
            $productservice.searchLive($scope.searchText).then(function(result)
            {
                console.log(result);
                if(result.length==0)
                {
                    $scope.entries="";

                }
                else {

                    $scope.entries = result;

                }
            });
        }

    };
/*The call for the shop button executes on line 65-87*/
    $scope.searchAll=function(){

        $rootScope.$broadcast('incCart',{"cartnum":0});
        if($scope.searchText && ($scope.startdate || $scope.enddate))
        {
            localStorage.setItem('searchdata',$scope.searchText);
            localStorage.setItem('searchbdate',$scope.startdate);
            localStorage.setItem('searchEndate',$scope.enddate);
            $productservice.getSearch($scope.searchText,$scope.startdate,$scope.enddate).then(function(result){
                localStorage.setItem("searchResult",JSON.stringify(result));
                $location.path('/mobiles');
            });

        }
        else if($scope.searchText && !$scope.startdate && !$scope.enddate )
        {
            localStorage.setItem('searchdata',$scope.searchText);
            $productservice.getSearch($scope.searchText).then(function(result){
                localStorage.setItem("searchResult",JSON.stringify(result));
                $location.path('/mobiles');
            });
        }
        else
        {
            $productservice.getAll().then(function(result){

                for(var i=0;i<result.length;i++)
                {
                    result[i].cartqt=0;
                    result[i].product=1;
                }
                localStorage.setItem('searchResult',JSON.stringify(result));
                $accessoryService.getAll().then(function (result2) {
                    for(var i=0;i<result2.length;i++)
                    {
                        result2[i].cartqt=0;
                        result2[i].product=0;
                    }
                    localStorage.setItem('viewAccessory',JSON.stringify(result2));
                },function (error) {
                    console.error(error);
                });
                localStorage.setItem("searchResult",JSON.stringify(result));
                $location.path('/mobiles');
            },function (error) {
               console.error(error);
            });
        }

    };
}]);
