/**
 * Created by plenarea-2 on 21/10/16.
 */
var app = angular.module('myApp.mobiles', ['ui.bootstrap','angular-jwt','bootstrapLightbox','toaster']);
app.controller("mobilesController",['$scope','$routeParams','$rootScope','$AuthService','$route','$userService','Pagination','$productservice','$location','Lightbox','$modal','toaster',function($scope,$routeParams,$rootScope,$AuthService, $route,$userService,Pagination,$productservice,$location,Lightbox,$modal,toaster)
{
if(localStorage.getItem('searchdata'))
{

    $scope.search=localStorage.getItem('searchdata');

}

    $scope.$route = $route;
    $scope.isLogIn=false;
 $scope.navigate=localStorage.getItem("navigate");
    if(localStorage.getItem("token")!=null)
    {
        $scope.isLogIn=true;
        $productservice.getUserData().then(function(response){
            pageData(response);

        },function(err){
            console.log("Not found Data");
        });
    }
    else{
        var result= JSON.parse(localStorage.getItem('searchResult'));


        if(result){
            pageData(result);
        }else {
            $location.path('/');
        }


    }


    $scope.getDetail=function(id)
    { if(localStorage.getItem("token")==null) {
        $productservice.getDetailById(id).then(function (response) {
            var detailsResult=[]=response;
            var cartqt;
            var result= JSON.parse(localStorage.getItem("searchResult"));
            for(var i=0;i<result.length;i++)
            {
                if(result[i].id==id)
                {
                    if(result[i].cartqt==0)
                    {
                        cartqt=0;
                        break;
                    }
                    else
                    {
                        cartqt=result[i].cartqt;
                        
                        break;
                    }

                }
            }
            for (var i=0;i<detailsResult.length;i++)
            {
                detailsResult[i].cartqt=cartqt;
            }
            localStorage.setItem('viewdetils', JSON.stringify(detailsResult));
            localStorage.setItem('product','mobiles');
                $location.path('/configuration');


            for(var i=0;i<result.length;i++)
                if(result[i].id===id)
                {
                    result[i].visited=true;
                    localStorage.setItem('searchResult', JSON.stringify(result));
                    break;
                }

        }, function (err) {
            alert(err);
        });
    }
    else{
        return false;
    }
    };

    function pageData(response)
    {
        $scope.list=response;
        $scope.pagination = Pagination.getNew(8);
        var x=$scope.pagination.numPages=Math.ceil($scope.list.length/$scope.pagination.perPage);
        var st=[];
        for(i=0;i<x;i++)
        {
            st.push(i);
        }
        $scope.arry=st;
    };

    $scope.setSearch=function (search) {
        if($scope.search.length==0){
            $scope.phones=null;

        }
        else {
            var keyword=$scope.search;
            $productservice.searchByKeyword(keyword).then(function (data) {
            $scope.phones=data;

            })
        }
    };

    $scope.getSearch=function (search) {

      if($scope.search.length==0)
      {
          var result= JSON.parse(localStorage.getItem('searchResult'));
          pageData(result);
      }
      else
      {
          var keyword=$scope.search;
          $productservice.searchByKeyword(keyword).then(function (data) {

              var result= JSON.parse(localStorage.getItem('searchResult'));


           for(var i=0;i<result.length;i++) {

               for (var j = 0; j < data.length; j++) {
                   if(result[i].id==data[j].id) {
                       data[j].cartqt = result[i].cartqt;
                       data[j].product = result[i].product;
                       if(result[i].visited)
                       {
                           data[j].visited=result[i].visited;
                       }
                   }

               }
           }
              localStorage.removeItem('searchP');
              localStorage.setItem('searchP',JSON.stringify(data));
              localStorage.setItem('searchQuery',$scope.search);
              localStorage.setItem('localdata','product');
              $location.path('/search');

          })

      }
    };


    var cart=[];
        $scope.addCart=function (data,id) {

            if(!localStorage.getItem('cart')) {
                data.cartqt=1;
                cart.push(data);
                var addNum;
                localStorage.setItem('cart', JSON.stringify(cart));
                if($rootScope.cartnum===undefined)
                {
                    addNum=0;
                }
                else {
                    addNum=$rootScope.cartnum;
                }
                addNum=addNum+1;
                $rootScope.$broadcast('incCart',{"cartnum":addNum});
                localStorage.setItem('cartNum',addNum);
                var result1= JSON.parse(localStorage.getItem("searchResult"));
                for(var i=0;i<result1.length;i++)
                    if(result1[i].id===id)
                    {
                        result1[i].cartqt=1;
                        localStorage.setItem('searchResult', JSON.stringify(result1));
                        break;
                    }
                toaster.success({body:"Product Added"});
            }
            else
            {

                var test=JSON.parse(localStorage.getItem('cart'));
                var addNum2;
                data.cartqt=1;
                test.push(data);
                localStorage.setItem('cart', JSON.stringify(test));
                if($rootScope.cartnum===undefined)
                {
                    addNum2=0;
                }
                else {
                    addNum2=$rootScope.cartnum;
                }
                addNum2=addNum2+1;

                $rootScope.$broadcast('incCart',{"cartnum":addNum2});
                localStorage.setItem('cartNum',addNum2);
                var result= JSON.parse(localStorage.getItem("searchResult"));
                for(var i=0;i<result.length;i++)
                    if(result[i].id===id)
                    {
                        result[i].cartqt=1;
                        localStorage.setItem('searchResult', JSON.stringify(result));
                        break;
                    }
                toaster.success({body:"Product Added"});
            }


        };
        $scope.openCart=function () {
            $location.path('/cart');
        }

}]);







