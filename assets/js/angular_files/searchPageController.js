/**
 * Created by plenarea on 23/2/17.
 */
var app = angular.module('myApp.search', ['ui.bootstrap','angular-jwt','bootstrapLightbox','toaster']);
app.controller("searchPageController",['$scope','$routeParams','$rootScope','$AuthService','$route','$userService','Pagination','$productservice','$accessoryService','$location','Lightbox','$modal','toaster',function($scope,$routeParams,$rootScope,$AuthService, $route,$userService,Pagination,$productservice,$accessoryService,$location,Lightbox,$modal,toaster)
{



        var result= JSON.parse(localStorage.getItem('searchP'));


        if(result){
            pageData(result);
        }



if(localStorage.getItem('localdata')=='product')
{
    var localdata='searchResult';
}
else if(localStorage.getItem('localdata')=='accessory')
{
    var localdata='viewAccessory';
}

    $scope.getDetail=function(id)
    { if(localStorage.getItem("token")==null) {
        if(localdata=='searchResult')
        {
           var service=$productservice;
        }
        if(localdata=='viewAccessory')
        {
            var service=$accessoryService;
        }
        service.getDetailById(id).then(function (response) {
            var detailsResult=[]=response;
            var cartqt;
            var result= JSON.parse(localStorage.getItem(localdata));
            for(var i=0;i<result.length;i++)
            {
                if(result[i].id==id)
                {
                    if(result[i].cartqt==0)
                    {
                        cartqt=0;
                        break;
                    }
                    else
                    {
                        cartqt=result[i].cartqt;
                        break;
                    }

                }
            }
            var result1= JSON.parse(localStorage.getItem("searchP"));
            for(var i=0;i<result1.length;i++)
            {
                if(result1[i].id==id)
                {
                    if(result1[i].cartqt==0)
                    {
                        cartqt=0;
                        break;
                    }
                    else
                    {
                        cartqt=result1[i].cartqt;
                        break;
                    }

                }
            }



            for (var i=0;i<detailsResult.length;i++)
            {
                detailsResult[i].cartqt=cartqt;
            }
            localStorage.setItem('viewdetils', JSON.stringify(detailsResult));
            localStorage.setItem('product','mobiles');
            $location.path('/configuration');


            for(var i=0;i<result.length;i++)
                if(result[i].id===id)
                {
                    result[i].visited=true;
                    localStorage.setItem(localdata, JSON.stringify(result));
                    break;
                }
            for(var i=0;i<result1.length;i++)
                if(result1[i].id===id)
                {
                    result1[i].visited=true;
                    localStorage.setItem('searchP', JSON.stringify(result1));
                    break;
                }

        }, function (err) {
            alert(err);
        });
    }
    else{
        return false;
    }
    };

    function pageData(response)
    {
        $scope.list=response;
        $scope.pagination = Pagination.getNew(4);
        var x=$scope.pagination.numPages=Math.ceil($scope.list.length/$scope.pagination.perPage);
        var st=[];
        for(i=0;i<x;i++)
        {
            st.push(i);
        }
        $scope.arry=st;
    };

$scope.search=localStorage.getItem('searchQuery');

    $scope.setSearch=function (search) {
        if(localdata=='searchResult')
        {
            var service=$productservice;
        }
        if(localdata=='viewAccessory')
        {
            var service=$accessoryService;
        }

        if($scope.search.length==0){
            $scope.phones=null;

        }
        else {
            var keyword=$scope.search;
            service.searchByKeyword(keyword).then(function (data) {
                $scope.phones=data;

            })
        }
    };

    $scope.getSearch=function (search) {
        if(localdata=='searchResult')
        {
            var service=$productservice;
        }
        if(localdata=='viewAccessory')
        {
            var service=$accessoryService;
        }
        if($scope.search.length==0)
        {
            var result= JSON.parse(localStorage.getItem(localdata));
            pageData(result);
        }
        else
        {
            var keyword=$scope.search;
            service.searchByKeyword(keyword).then(function (data) {

                var result= JSON.parse(localStorage.getItem(localdata));


                for(var i=0;i<result.length;i++) {

                    for (var j = 0; j < data.length; j++) {
                        if(result[i].id==data[j].id) {
                            data[j].cartqt = result[i].cartqt;
                            data[j].product = result[i].product;
                            if(result[i].visited)
                            {
                                data[j].visited=result[i].visited;
                            }
                        }

                    }
                }


                pageData(data);

            })

        }
    };


    var cart=[];
    $scope.addCart=function (data,id) {

        if(!localStorage.getItem('cart')) {
            data.cartqt=1;
            cart.push(data);
            var addNum;
            localStorage.setItem('cart', JSON.stringify(cart));
            if($rootScope.cartnum===undefined)
            {
                addNum=0;
            }
            else {
                addNum=$rootScope.cartnum;
            }
            addNum=addNum+1;
            $rootScope.$broadcast('incCart',{"cartnum":addNum});
            localStorage.setItem('cartNum',addNum);
            var result1= JSON.parse(localStorage.getItem(localdata));
            for(var i=0;i<result1.length;i++)
                if(result1[i].id===id)
                {
                    result1[i].cartqt=1;
                    localStorage.setItem(localdata, JSON.stringify(result1));
                    break;
                }
            var searchresult= JSON.parse(localStorage.getItem(localdata));
            for(var i=0;i<searchresult.length;i++)
                if(searchresult[i].id===id)
                {
                    searchresult[i].cartqt=1;
                    localStorage.setItem('searchP', JSON.stringify(searchresult));
                    break;
                }
            toaster.success({body:"Product Added"});
        }
        else
        {

            var test=JSON.parse(localStorage.getItem('cart'));
            var addNum2;
            data.cartqt=1;
            test.push(data);
            localStorage.setItem('cart', JSON.stringify(test));
            if($rootScope.cartnum===undefined)
            {
                addNum2=0;
            }
            else {
                addNum2=$rootScope.cartnum;
            }
            addNum2=addNum2+1;

            $rootScope.$broadcast('incCart',{"cartnum":addNum2});
            localStorage.setItem('cartNum',addNum2);
            var result= JSON.parse(localStorage.getItem(localdata));
            for(var i=0;i<result.length;i++)
                if(result[i].id===id)
                {
                    result[i].cartqt=1;
                    localStorage.setItem(localdata, JSON.stringify(result));
                    break;
                }
            var searchresult1= JSON.parse(localStorage.getItem(localdata));
            for(var i=0;i<searchresult1.length;i++)
                if(searchresult1[i].id===id)
                {
                    searchresult1[i].cartqt=1;
                    localStorage.setItem('searchP', JSON.stringify(searchresult1));
                    break;
                }
            toaster.success({body:"Product Added"});
        }


    };
    $scope.openCart=function () {
        $location.path('/cart');
    }

}]);
