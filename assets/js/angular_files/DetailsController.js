/**
 * Created by plenarea on 31/1/17.
 */
var app = angular.module('myApp.details', ['ui.bootstrap','angular-jwt','bootstrapLightbox']);
app.controller("DetailsController",['$scope','$routeParams','$rootScope','$AuthService','$route','$userService','$accessoryService','$location','$modal','toaster',function($scope,$routeParams,$rootScope,$AuthService, $route,$userService,$accessoryService,$location,$modal,toaster)
{
    $scope.navigate=localStorage.getItem("navigate");

    $scope.detilsInformation=JSON.parse(localStorage.getItem('viewdetils'));
    console.log($scope.detilsInformation);
    $scope.product=localStorage.getItem('product');



console.log($scope.detilsInformation);
    $scope.myInterval = -5;
    $scope.thumbnailSize = 3;
    $scope.thumbnailPage = 1;
 $scope.slides = [];
  var imagpath;
  var images=[];
if($scope.detilsInformation[0].listing_photos) {
    if ($scope.detilsInformation[0].listing_photos.length > 0) {

        var filename = $scope.detilsInformation[0].listing_photos;
        for (var x in filename) {
            images.push(filename[x].filename);
        }
        for (var i = 0; i < images.length; i++) {
            addSlide(i);
        }
    }
    else{
        if($scope.detilsInformation[0].condo_picture) {
            imagpath = $scope.detilsInformation[0].condo_picture;
        }
        for (var i=0; i<1; i++) {
            addSlide(i);
        }
    }
}

 else{

     if($scope.detilsInformation[0].picture) {
         imagpath = $scope.detilsInformation[0].picture;
     }
     else if($scope.detilsInformation[0].tour_picture) {
         imagpath = $scope.detilsInformation[0].tour_picture
     }
     for (var i=0; i<1; i++) {
         addSlide(i);
     }
 }
    function addSlide(i) {
        var newWidth = 600 + $scope.slides.length;

        if($scope.detilsInformation[0].listing_photos)
        {
            if($scope.detilsInformation[0].listing_photos.length>0) {
                $scope.slides.push({

                    image: images[i],
                    text: ['More', 'Extra', 'Lots of', 'Surplus'][$scope.slides.length % 4] + ' ' +
                    ['Cats', 'Kittys', 'Felines', 'Cutes'][$scope.slides.length % 4],
                    index: i
                });
            }
            else{
                $scope.slides.push({

                    image: imagpath,
                    text: ['More','Extra','Lots of','Surplus'][$scope.slides.length % 4] + ' ' +
                    ['Cats', 'Kittys', 'Felines', 'Cutes'][$scope.slides.length % 4],
                    index: i
                });
            }

        }else {
            $scope.slides.push({

                image: imagpath,
                text: ['More','Extra','Lots of','Surplus'][$scope.slides.length % 4] + ' ' +
                ['Cats', 'Kittys', 'Felines', 'Cutes'][$scope.slides.length % 4],
                index: i
            });
        }
    };




        $scope.tab='Configuration';
        $scope.titleName =
            {title1: 'Configuration', title2: 'Description', title3: 'Warranty'};


    $scope.tabHead=function (tabH) {
        $scope.tab=tabH;

    };


    $scope.prevPage = function(){
        if ($scope.thumbnailPage > 1){
            $scope.thumbnailPage--;
        }
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage-1)*$scope.thumbnailSize,$scope.thumbnailPage*$scope.thumbnailSize);
    };

    $scope.nextPage = function(){
        if ($scope.thumbnailPage <= Math.floor($scope.slides.length/$scope.thumbnailSize)){
            $scope.thumbnailPage++;
        }
        $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage-1)*$scope.thumbnailSize,$scope.thumbnailPage*$scope.thumbnailSize);
    };

    $scope.showThumbnails = $scope.slides.slice(($scope.thumbnailPage-1)*$scope.thumbnailSize,$scope.thumbnailPage*$scope.thumbnailSize);
    $scope.setActive = function(idx) {
        $scope.slides[idx].active=true;
    };

    $scope.addCart=function () {
        var product=localStorage.getItem('product');

        var result;
        if(product=='mobiles') {
             result = JSON.parse(localStorage.getItem('searchResult'));
        }
        else if(product=='accessory')
        {
            result = JSON.parse(localStorage.getItem('viewAccessory'));
        }
        for(var i=0;i<result.length;i++)
        {
            if(result[i].id==$scope.detilsInformation[0].id) {
                result[i].cartqt = 1;

                break;
            }
        }
        if(product=='mobiles') {
            localStorage.setItem('searchResult',JSON.stringify(result));
            $scope.detilsInformation[0].cartqt = 1;
            $scope.detilsInformation[0].product=1;
        }
        else if(product=='accessory')
        {
            localStorage.setItem('viewAccessory',JSON.stringify(result));
            $scope.detilsInformation[0].cartqt =  $scope.qty;
            $scope.detilsInformation[0].product=0;
        }
        var cart=[];
        if(!localStorage.getItem('cart')) {

            cart.push($scope.detilsInformation[0]);
            var addNum;
            localStorage.setItem('cart', JSON.stringify(cart));
            if ($rootScope.cartnum === undefined) {
                addNum = 0;
            }
            else {
                addNum = $rootScope.cartnum;
            }
            addNum = addNum + 1;
            $rootScope.$broadcast('incCart', {"cartnum": addNum});
            localStorage.setItem('cartNum', addNum);
            toaster.success({body:"Product Added"});
        }
        else{
            var test=JSON.parse(localStorage.getItem('cart'));
            var addNum2;
            test.push($scope.detilsInformation[0]);
            localStorage.setItem('cart', JSON.stringify(test));
            if($rootScope.cartnum===undefined)
            {
                addNum2=0;
            }
            else {
                addNum2=$rootScope.cartnum;
            }
            addNum2=addNum2+1;

            $rootScope.$broadcast('incCart',{"cartnum":addNum2});
            localStorage.setItem('cartNum',addNum2);
            toaster.success({body:"Product Added"});
        }
    };
    $scope.qty=1;
    $scope.calc=function (qty) {
        $scope.qty=qty;
    };

    $scope.openCart=function () {
        $location.path('/cart');
    };
    $scope.getrelated=function (sku) {
        $accessoryService.getRelatedAccessory(sku).then(function (result) {
            for(var i=0;i<result.length;i++)
            {
                result[i].cartqt=0;
                result[i].product=0;
            }
            localStorage.setItem('relatedAccessory',JSON.stringify(result));
            $location.path('/related');
        })
    };

    $scope.back=function () {
        if(localStorage.getItem('product')=='mobiles') {
            $location.path('/mobiles');
        }
        if(localStorage.getItem('product')=='accessory') {
            $location.path('/accessories');
        }
    }
}]);