/**
 * Created by plenarea-2 on 22/12/16.
 */
angular.module('myApp.productservice',[]).service('$productservice',['$http','$q','serverUrl',function($http,$q,serverUrl){


    var navigate='products';
    return{


        getDetailById:function(id)
        {
            var detail=$q.defer();
            $http.get(serverUrl+navigate+"/details/"+id, {
            }).success(function(data, status, headers, config) {
                detail.resolve(data);
            }).error(function(data, status, headers, config) {
                detail.reject(data);
            });
            return detail.promise;
        },
        searchByKeyword:function(keyword)
        {
            var deferred=$q.defer();
            $http.get(serverUrl+navigate+"/search/"+keyword,{

            }).success(function(data,status,headers,config){
                deferred.resolve(data);
            }).error(function(data,status,header,config)
            {
                deferred.reject(data);
            });
            return deferred.promise;

        },
        getAll:function(){
            var deferred=$q.defer();
            $http.get(serverUrl+ navigate,{

            }).success(function(data,status,headers,config){
                deferred.resolve(data);
            }).error(function(data,status,header,config)
            {
                deferred.reject(data);
            });
            return deferred.promise;

        }
    }

}]);

