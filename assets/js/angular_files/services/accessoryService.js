/**
 * Created by plenarea on 15/2/17.
 */
angular.module('myApp.accessoryService',[]).service('$accessoryService',['$http','$q','serverUrl',function($http,$q,serverUrl){

    var route='accessories';

    return {
        getAll: function () {
            var deferred = $q.defer();
            $http.get(serverUrl +route, {})
                .success(function (data, status, headers, config) {
                    deferred.resolve(data)
                }).error(function (data, status, headers, config) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getDetailById:function(id)
        {
            var detail=$q.defer();
            $http.get(serverUrl+route+"/details/"+id, {
            }).success(function(data, status, headers, config) {
                detail.resolve(data);
            }).error(function(data, status, headers, config) {
                detail.reject(data);
            });
            return detail.promise;
        },
        getRelatedAccessory:function (sku) {
            var detail=$q.defer();
            $http.get(serverUrl+route+"/search/downstream/"+sku, {
            }).success(function(data, status, headers, config) {
                detail.resolve(data);
            }).error(function(data, status, headers, config) {
                detail.reject(data);
            });
            return detail.promise;
        },
        searchByKeyword:function(keyword)
        {
            var deferred=$q.defer();
            $http.get(serverUrl+route+"/search/"+keyword,{

            }).success(function(data,status,headers,config){
                deferred.resolve(data);
            }).error(function(data,status,header,config)
            {
                deferred.reject(data);
            });
            return deferred.promise;

        }
    }


}]);
