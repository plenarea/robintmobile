/**
 * Created by plenarea on 20/2/17.
 */
angular.module('myApp.checkoutService',[]).service('$checkoutService',['$http','$q','serverUrl',function($http,$q,serverUrl){

    var route='checkout';

    return {
       postCheckout:function (cart,customer) {
           var deferred=$q.defer();
           $http.post(serverUrl+route+"/submit/all/",{
               cart:cart,
               customer:customer
           }).success(function(data,status,headers,config){
               deferred.resolve(data);

           }).error(function(data,status,header,config)
           {
               deferred.reject(data);
           });
           return deferred.promise;

       },

       postPersonalInfo:function (customer) {
           var deferred=$q.defer();
           $http.post(serverUrl+route+"/personalinfo/",{
                firstname:customer.firstname,
                lastname:customer.lastname,
                email:customer.email,
                phone:customer.phone,
                currentcarrier:customer.currentcarrier
           }).success(function(data,status,headers,config){
               deferred.resolve(data);

           }).error(function(data,status,header,config)
           {
               deferred.reject(data);
           });
           return deferred.promise;

       },
        postBillandShip:function (customer) {
            var deferred=$q.defer();
            $http.post(serverUrl+route+"/billandship/",{
                orderid:''+customer.orderid+'',
                shiptype:'UPS',
                shipaddress1:customer.shipaddress1,
                shipaddress2:customer.shipaddress2,
                billaddress1:customer.billaddress1,
                billaddress2:customer.billaddress2,
                shipstate:customer.shipstate,
                shipcity:customer.shipcity,
                billstate:customer.billstate,
                billcity:customer.billcity,
                shipzip:customer.shipzip,
                billzip:customer.billzip,
                cardno:customer.cardno,
                expirydate:customer.expirydate,
                cvv:customer.cvv,
                store:customer.store

            }).success(function(data,status,headers,config){
                deferred.resolve(data);

            }).error(function(data,status,header,config)
            {
                deferred.reject(data);
            });
            return deferred.promise;

        },
        postCreditrating:function (customer,cart) {
            var deferred=$q.defer();
            cart.id=customer.orderid;
            $http.post(serverUrl+route+"/creditinfo/",{
                customer:customer,
                cart:cart
            }).success(function(data,status,headers,config){
                deferred.resolve(data);

            }).error(function(data,status,header,config)
            {
                deferred.reject(data);
            });
            return deferred.promise;

        }

    }


}]);
