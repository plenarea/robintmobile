/**
 * Created by plenarea-2 on 2/12/16.
 */
angular.module("myApp.service",[]).service('$userService',['$http','$q','jwtHelper','$location','serverUrl',function($http,$q,jwtHelper,$location,serverUrl){
    var username;
    var deferred = $q.defer();
    return{

        getListingDetailById:function(id)
        {
            $http.get(serverUrl+'listings/details/'+id, {
            }).success(function(data, status, headers, config) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                deferred.reject(data);
            });
           return deferred.promise;
        },
        getUserUnderList:function()
        {
            $http.get(serverUrl+'listings',{
            }).success(function (data,status,headers,config) {
                deferred.resolve(data);
            }).error(function(data,status,header,config)
            {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        checkSession:function(){
            if(localStorage.getItem('token'))
            {
                 username = jwtHelper.decodeToken(localStorage.getItem('token')).username;
                return username;
            }else
            {
                $location.path('/');
            }
        },
        sendMailOwner:function (data) {
            var deferred=$q.defer();
            navigate=localStorage.getItem("navigate");
            $http.post(serverUrl+"sendMailtoOwner",data).success(function(data,status,headers,config){
                deferred.resolve(data);

            }).error(function(data,status,header,config)
            {
                deferred.reject(data);
            });
            return deferred.promise;
        }
    }
}]);