/**
 * Created by plenarea-2 on 3/11/16.
 */
angular.module('myApp.factories.auth', [])

    .service('$AuthService',['$http','$location','$rootScope','serverUrl' ,function($http, $location,$rootScope,serverUrl) {

        return {
            login: function (user) {
                $http.post(serverUrl+'login',{
                    username:user.username,
                    password:user.password
                })
                    .then(function(res){
                        if(res){
                            console.log(res);
                            $location.path('/excursions');
                            localStorage.removeItem('token');
                            localStorage.setItem('token',res.data);
                            $rootScope.$broadcast('isLogin',{"activate":true,"username":user.username});

                        }else{
                            $location.path('/');
                        }
                    }).catch(function(data){
                        console.log("Error: ",data);
                    })
            },
            logout:function()
            {
                $http.get(serverUrl+'logout',{

                }).then(function(res){
                        if(res){
                            $rootScope.$broadcast('clear',false);
                            localStorage.clear();
                            $location.path('/');

                        }
                    }).catch(function(data){
                        console.log("Error: ",data);
                    localStorage.clear();
                    $location.path('/');
                    })
            },
            register: function (user) {
                $http.post(serverUrl+'register',{
                    username:user.username,
                    password:user.password
                })
                    .then(function(res){
                        if(res){
                            console.log(res);
                            alert("Your Registration Completed");
                            $location.path('/login');


                        }else{
                            $location.path('/register');
                        }
                    }).catch(function(data){
                        console.log("Error: ",data);
                    })
            }


        }
    }]);









