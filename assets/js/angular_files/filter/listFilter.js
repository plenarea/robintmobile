/**
 * Created by plenarea on 1/2/17.
 */
angular.module('myApp.listingFilter',[]).filter('searchName', function() {
    return function(list,name) {
        var filtered = [];

        if(!name ){return list;}
        angular.forEach(list, function(item) {

            if(angular.lowercase(item.name).indexOf(angular.lowercase(name))!=-1)
            {
                filtered.push(item);
            }

        });
        return filtered;
    };
}).filter('searchDate',function () {
    return function(list,start,end) {
        var filtered = [];
        var d = new Date(start);
        var d2 = new Date(end);
        if(!start || !end){return list;}
        if(start && end) {
            angular.forEach(list, function (item) {
                if (item.listing_pricing) {
                    var d3 = new Date(item.listing_pricing.begin_date);
                    var d4 = new Date(item.listing_pricing.end_date);
                    if (d3 >= d && d4 <= d2) {
                        filtered.push(item);
                    }
                }


                /*if(angular.lowercase(item.name).indexOf(angular.lowercase(sleep))!=-1)
                 {
                 filtered.push(item);
                 }*/

            });
        }
        return filtered;
    };
}).filter('rateFilter',function () {
    return function(list,min,max) {
        var filtered = [];

        if (!min && !max) {
            return list;
        }
        angular.forEach(list, function (item) {
              if (item) {
                 if (item.retail_price>=min && item.retail_price<=max) {
                    filtered.push(item);
                    }
                }


                /*if(angular.lowercase(item.name).indexOf(angular.lowercase(sleep))!=-1)
                 {
                 filtered.push(item);
                 }*/

            });

        return filtered;
    }
}) ;