

var app = angular.module('myApp.cart', ['ui.bootstrap']);
app.controller("cartController",['$scope','$http','$routeParams','$rootScope','$location','$AuthService','jwtHelper', '$route','$userService','$accessoryService','toaster',function($scope,$http,$routeParams,$rootScope,$location,$AuthService,jwtHelper, $route,$userService,$accessoryService,toaster)
{
    $scope.$route = $route;
    var cartContents=JSON.parse(localStorage.getItem('cart'));
    $scope.cart=cartContents;
    $scope.rates=[];


    $scope.removeItem=function (index) {
        var n=$scope.cart[index].name;
        $scope.cart.splice(index,1);
        calculateremoval();
      var result=JSON.parse(localStorage.getItem('searchResult'));
        for(var i=0;i<result.length;i++)
        {
            if(result[i].name==n)
            {
                result[i].cartqt=0;
                break;
            }

        }
        localStorage.setItem('searchResult',JSON.stringify(result));
        var result1=JSON.parse(localStorage.getItem('viewAccessory'));
        for(var i=0;i<result1.length;i++)
        {
            if(result1[i].name==n)
            {
                result1[i].cartqt=0;
                break;
            }

        }
        localStorage.setItem('viewAccessory',JSON.stringify(result1));
      localStorage.setItem('cart',JSON.stringify($scope.cart));
      var removenum;
        if($rootScope.cartnum===undefined)
        {
            removenum=0;
        }
        else {
            removenum=$rootScope.cartnum;
        }
        removenum=removenum-1;

        $rootScope.$broadcast('incCart',{"cartnum":removenum});
        localStorage.setItem('cartNum',removenum);
        toaster.warning({body:"Product deleted"})
    };
    $scope.total=function (index) {
        var name=$scope.cart[index].name;
        var qty=$scope.cart[index].cartqt;
        var cartcnt=JSON.parse(localStorage.getItem('cart'));
        var total=0;
        for(var i=0;i<cartcnt.length;i++)
        {
            if(cartcnt[i].name==name)
            {
                cartcnt[i].cartqt=qty;
                break;
            }
        }
        for (var i=0;i<cartcnt.length;i++)
        {
           total=total+(cartcnt[i].sale_price*cartcnt[i].cartqt);
        }
        $scope.totalprice=total;
        localStorage.setItem(JSON.stringify(cartcnt));

    };

    if(localStorage.getItem('cart')==undefined || $scope.cart.length==0)
    {
        $scope.emptycart=true;
    }

    else if($scope.cart)
    {
        $scope.emptycart=false;
        var calculate=[];
        var total=0;
        calculate=$scope.cart;
        for(var i=0;i<calculate.length;i++)
        {
            total=total+(calculate[i].cartqt*calculate[i].sale_price);
        }
        $scope.totalprice=total;
    }
    calculateremoval=function () {
        if($scope.cart.length==0)
        {
            $scope.emptycart=true;
            $scope.totalprice='';
        }
        else
        {
            var calculate=[];
            var total=0;
            calculate=$scope.cart;
            for(var i=0;i<calculate.length;i++)
            {
                total=total+(calculate[i].cartqt*calculate[i].sale_price);
            }
            $scope.totalprice=total;
        }
    };

    $scope.getrelated=function (sku) {
        $accessoryService.getRelatedAccessory(sku).then(function (result) {
            for(var i=0;i<result.length;i++)
            {
                result[i].cartqt=0;
                result[i].product=0;
            }
            localStorage.setItem('relatedAccessory',JSON.stringify(result));
            $location.path('/related');
        })
    };



    $scope.checkout=function () {
        var newCart=[];
        var items=[];
        for(var i=0;i<$scope.cart.length;i++)
        {
           items.push({"id":$scope.cart[i].epid,"price":$scope.cart[i].sale_price});
        }
        $scope.items=items;
        newCart.push({"id":19,"items":$scope.items,"total":$scope.totalprice});
        localStorage.setItem('exitcart',JSON.stringify(newCart));
        $location.path('/info');
    }

}]);