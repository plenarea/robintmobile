/**
 * Created by plenarea-2 on 26/10/16.
 */
var app = angular.module('myApp.loginapp', []);
app.controller("LoginController",['$scope','$http','$location', '$AuthService','$route',function($scope,$http,$location, $AuthService,$route)
{


    $scope.$route = $route;
    $scope.login=function(event) {
        event.preventDefault();
        if( $scope.uname && $scope.psw)
            $AuthService.login({ username: $scope.uname, password: $scope.psw });

    };
    $scope.passNotMatch=false;
    $scope.registers=function()
    {

        if($scope.username &&  $scope.password && $scope.repeatpassword &&  $scope.password==$scope.repeatpassword)
        {
        $AuthService.register({ username: $scope.username, password: $scope.password });
        }else
        {
            $scope.passNotMatch=true;
        }

    };

}]);
