/**
 * Created by plenarea-1 on 18/2/17.
 */
var app = angular.module('myApp.order', ['ui.bootstrap']);
app.controller("OrderController",['$scope','$http','$routeParams','$rootScope','$location','$AuthService','jwtHelper', '$route','$checkoutService',function($scope,$http,$routeParams,$rootScope,$location,$AuthService,jwtHelper, $route,$checkoutService)
{
$scope.phone = /^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/;
$scope.firstname =/^[a-zA-Z ]{2,30}$/;
$scope.email = 'me@example.com';

$scope.customer_details ={};
$scope.checkaddress=true;
$scope.placeOrder = function(){
    $scope.customer_details.billaddress1=$scope.customer_details.shipaddress1;
    $scope.customer_details.billaddress2=$scope.customer_details.shipaddress2;
    $scope.customer_details.billcity=$scope.customer_details.shipcity;
    $scope.customer_details.billstate=$scope.customer_details.shipstate;
    $scope.customer_details.billzip=$scope.customer_details.shipzip;
    if($scope.customer_details.middlename!=null) {
        $scope.customer_details.customername = $scope.customer_details.firstname + " " + $scope.customer_details.middlename + " " + $scope.customer_details.lastname;
    }
    else {
        $scope.customer_details.customername = $scope.customer_details.firstname + " " + $scope.customer_details.lastname;
    }
    $scope.customer_details.currentcarrier='T-Mobile';
    $scope.customer_details.store='T-Mobile';
    $scope.customer_details.creditreportusagetype='SUPER-CREDIT';
    $scope.cart=JSON.parse(localStorage.getItem('exitcart'));
    localStorage.setItem('customername',$scope.customer_details.firstname);
    var checkout=[];
    checkout.push({"cart": $scope.cart,"customer":$scope.customer_details});
localStorage.setItem('checkout',JSON.stringify(checkout));
    $checkoutService.postCheckout($scope.cart[0],$scope.customer_details).then(function (response) {
        localStorage.setItem('orderid',response.order_id);
        $location.path('/success');
        $rootScope.$broadcast('incCart',{"cartnum":0});

    },function (error) {
        console.error(error);
    });
};
$scope.hide=0;
$scope.success1='';
$scope.success2='';
$scope.progress=function (id) {

    if(id==1)
    {
        $scope.customer_details.currentcarrier='T-Mobile';
        $scope.customer_details.store='USA';
        $checkoutService.postPersonalInfo($scope.customer_details).then(function (response) {
        $scope.customer_details.orderid=response.orderdetails[0].orderid;
        localStorage.setItem('orderid',response.orderdetails[0].orderid);
        localStorage.setItem('customername',$scope.customer_details.firstname);
        $scope.hide=id;
        $scope.success1='id1';
        },function (error) {
            console.log(error)
        })
    }
    else if(id==2)
    {
        $scope.cart=JSON.parse(localStorage.getItem('exitcart'));
        if($scope.checkaddress==true)
        {
            $scope.customer_details.billaddress1 = $scope.customer_details.shipaddress1;
            $scope.customer_details.billaddress2 = $scope.customer_details.shipaddress2;
            $scope.customer_details.billcity = $scope.customer_details.shipcity;
            $scope.customer_details.billstate = $scope.customer_details.shipstate;
            $scope.customer_details.billzip = $scope.customer_details.shipzip;
        }
        else {

        }
        $checkoutService.postBillandShip($scope.customer_details).then(function (response) {
            $scope.hide=id;
            $scope.success2='id2';
        },function (error) {
            console.log(error)
        })
    }
    else if(id==3)
    {
            $checkoutService.postCreditrating($scope.customer_details,$scope.cart[0]).then(function (response) {
            $rootScope.$broadcast('incCart',{"cartnum":0});
            localStorage.setItem('cartid',response.token);
            $location.path('/success');
        },function (error) {
            console.log(error)
        })
    }

};
    $scope.changeTab=function (id) {
        $scope.hide=id;
    };

    $scope.confirmemail=function () {

    };
    $scope.customername=localStorage.getItem('customername');
    $scope.orderid=localStorage.getItem('orderid');
    $scope.cartid=localStorage.getItem('cartid');
    $scope.home=function () {
        localStorage.clear();
        $location.path('/');
    }
}]);